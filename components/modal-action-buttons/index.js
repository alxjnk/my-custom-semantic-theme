import React from 'react';
import { Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import './styles.less';

const CLASS_NAME = 'modal-action-buttons';

const ModalActionButtons = ({ buttons }) => (
  <div className={CLASS_NAME}>
    {buttons.map(({ color, onClick, text, disabled, loading }) => (
      <div className={`${CLASS_NAME}__button-wrapper`} key={text}>
        <Button
          className={color || 'primary'}
          onClick={onClick}
          type="button"
          disabled={disabled}
          loading={loading}
        >
          {text}
        </Button>
      </div>
    ))}
  </div>
);

ModalActionButtons.propTypes = {
  buttons: PropTypes.arrayOf(
    PropTypes.shape({
      color: PropTypes.string,
      onClick: PropTypes.func.isRequired,
      text: PropTypes.string.isRequired,
      disabled: PropTypes.bool,
    }),
  ),
};

export { ModalActionButtons };
