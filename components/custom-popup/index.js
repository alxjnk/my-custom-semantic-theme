import React from 'react';
import { Popup } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import './styles.less';

function CustomPopup({ isPopupDisabled, trigger, headerText, content }) {
  return (
    <Popup
      position="right center"
      className="custom-popup"
      disabled={isPopupDisabled}
      mouseLeaveDelay={0}
      trigger={<div className="custom-popup__trigger">{trigger}</div>}
    >
      <Popup.Header className="custom-popup__header">{headerText}</Popup.Header>
      {content && (
        <Popup.Content className="custom-popup__content">
          {content}
        </Popup.Content>
      )}
    </Popup>
  );
}
CustomPopup.propTypes = {
  isPopupDisabled: PropTypes.bool,
  trigger: PropTypes.element.isRequired,
  headerText: PropTypes.string,
  content: PropTypes.element,
};

export { CustomPopup };
