import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
// import { ChevronDown } from 'tabler-icons-react';

export const ExcelButton = ({ onClick, text }) => (
  <Button icon color="green" onClick={onClick} size="medium">
    {text}
  </Button>
);

ExcelButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

export default ExcelButton;
