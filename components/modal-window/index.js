import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'semantic-ui-react';
import { ModalActionButtons } from '../modal-action-buttons';
import { ModalCloseButton } from './components/modal-close-button';
import './styles.less';

function ModalWindow({
  isModalShown,
  onClose,
  headerText = 'Modal Header',
  actionButtons,
  modalContent,
  size = 'mini',
  tourSelector,
  isNarrow,
  notShowCloseButton,
}) {
  return (
    <Modal
      size={size}
      open={isModalShown}
      onClose={onClose}
      className={`modal-window ${isNarrow ? 'modal-window_narrow' : ''}`}
      tour-data={tourSelector}
    >
      <Modal.Header className="modal-window__header">
        <p className="modal-window__header-text">{headerText}</p>
        {!notShowCloseButton && <ModalCloseButton onClick={onClose} />}
      </Modal.Header>
      {modalContent && <Modal.Content>{modalContent}</Modal.Content>}
      <Modal.Actions>
        <ModalActionButtons buttons={actionButtons} />
      </Modal.Actions>
    </Modal>
  );
}

ModalWindow.propTypes = {
  isModalShown: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  headerText: PropTypes.string,
  actionButtons: PropTypes.arrayOf(
    PropTypes.shape({
      color: PropTypes.string,
      onClick: PropTypes.func.isRequired,
      text: PropTypes.string.isRequired,
    }),
  ),
  modalContent: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  size: PropTypes.string,
  tourSelector: PropTypes.string,
  isNarrow: PropTypes.bool,
  notShowCloseButton: PropTypes.bool,
};

export { ModalWindow };
