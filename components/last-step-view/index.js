import React from 'react';
import PropTypes from 'prop-types';
import { TourLastStepButtons } from '../tour-last-step-buttons';
import './styles.less';

function LastStepView({ onClickCloseTour, onClickNextTour, text }) {
  return (
    <div className="last-step-view">
      {text}
      <TourLastStepButtons
        onClickNextTour={onClickNextTour}
        onClickCloseTour={onClickCloseTour}
      />
    </div>
  );
}
LastStepView.propTypes = {
  onClickNextTour: PropTypes.func,
  onClickCloseTour: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

export { LastStepView };
