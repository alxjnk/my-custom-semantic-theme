import React from 'react';
import { Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import './styles.less';

function FooterLayoutButton({ text, onClick, isDisabled, loading, form }) {
  return (
    <div className="footer-layout-button">
      <Button
        onClick={onClick}
        primary
        disabled={isDisabled}
        loading={loading}
        form={form}
        size="large"
      >
        {text}
      </Button>
    </div>
  );
}

FooterLayoutButton.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  isDisabled: PropTypes.bool,
  loading: PropTypes.bool,
  form: PropTypes.string,
};

export { FooterLayoutButton };
