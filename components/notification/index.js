import React, { useEffect } from 'react';
import { X } from 'tabler-icons-react';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import './styles.less';
import { notificationDismiss } from '../../../modules/notifications/actions';

const NotificationItem = ({ notificationInfo, notificationDismissFunc }) => {
  useEffect(() => {
    setTimeout(
      () => notificationDismissFunc({ id: notificationInfo.id }),
      notificationInfo.dismissAfter,
    );
  }, []);

  return (
    <div className={`notification notification-${notificationInfo.type}`}>
      <div className="notification__content">
        <p className="notification__message">{notificationInfo.message}</p>
        <p className="notification__time">{notificationInfo.initTime}</p>
      </div>
      <X
        size={24}
        className="notification__button"
        onClick={() => notificationDismissFunc({ id: notificationInfo.id })}
      />
    </div>
  );
};

NotificationItem.propTypes = {
  notificationInfo: PropTypes.object,
  notificationDismissFunc: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = { notificationDismissFunc: notificationDismiss };

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const Notification = compose(withConnect)(NotificationItem);

export { Notification };
