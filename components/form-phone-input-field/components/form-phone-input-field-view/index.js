import React from 'react';
import InputMask from 'react-input-mask';
import { Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import './styles.less';

const FormPhoneInputFieldView = ({
  input,
  type,
  placeholder,
  shouldErrorBeShown,
  errorMessage,
  label,
  autoFocus,
  autoComplete,
  mask,
}) => (
  <div className="input_row">
    {label && <label htmlFor={label}>{label}</label>}

    <InputMask
      {...input}
      value={input.value}
      autoFocus={autoFocus}
      fluid
      type={type}
      placeholder={placeholder}
      id={label}
      autoComplete={autoComplete}
      mask={mask}
      maskChar=""
      alwaysShowMask
    >
      {inputProps => <Input {...inputProps} />}
    </InputMask>
    {shouldErrorBeShown && (
      <span className="validation_error">{errorMessage}</span>
    )}
  </div>
);

FormPhoneInputFieldView.propTypes = {
  input: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired,
  shouldErrorBeShown: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  autoFocus: PropTypes.bool,
  autoComplete: PropTypes.oneOf(['off']),
  mask: PropTypes.string.isRequired,
};

export { FormPhoneInputFieldView };
