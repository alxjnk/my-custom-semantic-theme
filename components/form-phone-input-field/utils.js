export const getShouldErrorBeShown = (notShow, touched, error) => {
  if (notShow) {
    return false;
  }
  if (touched && error) {
    return touched && Boolean(error);
  }
  return false;
};
