import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { FormPhoneInputFieldView } from './components/form-phone-input-field-view';
import { getShouldErrorBeShown } from './utils';

const FormPhoneInputField = ({
  input,
  type,
  placeholder,
  meta: { touched, error },
  label,
  autoFocus,
  autoComplete,
  errorsNotShown,
  mask
}) => {
  const shouldErrorBeShown = useMemo(
    () => getShouldErrorBeShown(errorsNotShown, touched, error),
    [errorsNotShown, touched, error],
  );

  return (
    <FormPhoneInputFieldView
      shouldErrorBeShown={shouldErrorBeShown}
      autoFocus={autoFocus}
      autoComplete={autoComplete}
      label={label}
      placeholder={placeholder}
      type={type}
      input={input}
      errorMessage={error}
      mask={mask}
    />
  );
};

FormPhoneInputField.propTypes = {
  input: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  autoFocus: PropTypes.bool,
  autoComplete: PropTypes.oneOf(['off']),
  errorsNotShown: PropTypes.bool,
  mask: PropTypes.string.isRequired
};

export { FormPhoneInputField };
