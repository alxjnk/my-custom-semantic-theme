import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const MobileTabs = ({ active, tabData, size, onTabClick, tabColor }) => (
  <div className="mobile-tabs">
    {tabData.map(({ icon: Icon, name, notice }) => (
      <div
        key={name}
        className={`mobile-tabs__item ${
          notice ? 'mobile-tabs__item_notice' : ''
        }`}
        onClick={() => onTabClick(name)}
        role="button"
        tabIndex={-1}
        onKeyPress={null}
        tour-data={`tabs__${name}`}
      >
        <Icon
          size={size}
          color={active === name ? tabColor.active : tabColor.disabled}
        />
      </div>
    ))}
  </div>
);

MobileTabs.propTypes = {
  active: PropTypes.string.isRequired,
  tabData: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      icon: PropTypes.elementType.isRequired,
      notice: PropTypes.bool,
    }),
  ),
  size: PropTypes.number.isRequired,
  onTabClick: PropTypes.func,
  tabColor: PropTypes.shape({
    active: PropTypes.string.isRequired,
    disabled: PropTypes.string.isRequired,
  }).isRequired,
};
export { MobileTabs };
