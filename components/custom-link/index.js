import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './styles.less';

function CustomLink({ text, to }) {
  return (
    <Link className="custom-link" to={to}>
      {text}
    </Link>
  );
}

CustomLink.propTypes = {
  text: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

export { CustomLink };
