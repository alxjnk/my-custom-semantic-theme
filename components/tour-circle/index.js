import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

function TourCircle({ step, onClick }) {
  return (
    <div className="tour-circle" onClick={onClick}>
      {step}
    </div>
  );
}

TourCircle.propTypes = {
  step: PropTypes.number,
  onClick: PropTypes.func.isRequired,
};
export { TourCircle };
