import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

function OutlineCustomButton({ onClick, icon: Icon }) {
  return (
    <div
      className="outline-custom-button"
      onClick={onClick}
      role="button"
      tabIndex={0}
      onKeyPress={onClick}
    >
      <Icon color="grey" />
    </div>
  );
}

OutlineCustomButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  icon: PropTypes.elementType.isRequired,
};

export { OutlineCustomButton };
