import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { MobileListItem } from '../mobile-list-item';
import './styles.less';

const MobileList = ({ data, icon }) => (
  <div className="mobile-list">
    {data &&
      data.map(item => (
        <Link to={item.to} key={`${item.id}`}>
          <MobileListItem
            avatarText={item.avatarText}
            avatar={item.avatar}
            img={item.avatarImage}
            title={item.title}
            subText={item.subText}
            icon={icon}
          />
        </Link>
      ))}
  </div>
);

MobileList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      text: PropTypes.string,
      subText: PropTypes.string,
      img: PropTypes.string,
      id: PropTypes.string.isRequired,
      to: PropTypes.string.isRequired,
    }),
  ).isRequired,
  icon: PropTypes.element,
};

export { MobileList };
