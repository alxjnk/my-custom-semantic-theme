import React from 'react';
import './styles.less';

function HSLogo() {
  return <div className="housekeeping-logo">H</div>;
}

export { HSLogo };
