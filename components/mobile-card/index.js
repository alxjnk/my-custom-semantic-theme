import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import './styles.less';

function MobileCard({
  mainText,
  subTextArray,
  avatar: Avatar,
  avatarName,
  avatarImg,
}) {
  const { t } = useTranslation();
  return (
    <div className="mobile-card">
      <div className="mobile-card__card card">
        {Avatar && <Avatar name={avatarName} img={avatarImg} />}
        <div className="mobile-card__info">
          <p className="mobile-card__info-text">{mainText}</p>
          {subTextArray.map(el => (
            <p className="mobile-card__info-subtext" key={el.text}>{`${t(
              el.text,
            )}: ${el.data}`}</p>
          ))}
        </div>
      </div>
    </div>
  );
}

MobileCard.propTypes = {
  mainText: PropTypes.string.isRequired,
  subTextArray: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      data: PropTypes.string.isRequired,
    }),
  ),
  avatar: PropTypes.elementType,
  avatarName: PropTypes.string,
  avatarImg: PropTypes.string,
};

export { MobileCard };
