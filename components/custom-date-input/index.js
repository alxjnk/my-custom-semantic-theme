import React from 'react';
import PropTypes from 'prop-types';
import { DateInput } from 'semantic-ui-calendar-react';

function CustomDateInput({
  selectedDate,
  onChangeDate,
  markedDate,
  minDate,
  localization,
  isInline,
  placeholder,
  maxDate,
}) {
  return (
    <div className="custom-date-input">
      <DateInput
        localization={localization}
        inline={isInline}
        value={selectedDate}
        onChange={onChangeDate}
        dateFormat="YYYY-MM-DD"
        marked={markedDate}
        minDate={minDate}
        maxDate={maxDate}
        animation="none"
        closable
        placeholder={placeholder}
      />
    </div>
  );
}

CustomDateInput.propTypes = {
  selectedDate: PropTypes.string.isRequired,
  onChangeDate: PropTypes.func.isRequired,
  markedDate: PropTypes.arrayOf(PropTypes.instanceOf(Date)),
  minDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  maxDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  localization: PropTypes.string,
  isInline: PropTypes.bool,
  placeholder: PropTypes.string,
};

export { CustomDateInput };
