import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import './styles.less';

function TourLastStepButtons({ onClickCloseTour, onClickNextTour }) {
  return (
    <div className="tour-last-step-buttons">
      <div className="tour-last-step-buttons__button">
        <Button secondary onClick={onClickCloseTour}>
          Закрыть
        </Button>
      </div>
      {onClickNextTour && (
        <div className="tour-last-step-buttons__button">
          <Button primary onClick={onClickNextTour}>
            Продолжить
          </Button>
        </div>
      )}
    </div>
  );
}
TourLastStepButtons.propTypes = {
  onClickNextTour: PropTypes.func,
  onClickCloseTour: PropTypes.func.isRequired,
};

export { TourLastStepButtons };
