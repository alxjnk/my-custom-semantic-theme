import React from 'react';
import PropTypes from 'prop-types';
import { QuestionMark } from 'tabler-icons-react';
import './styles.less';

function StartTourIcon({ onStartTour }) {
  return (
    <div className="start-tour-icon" onClick={onStartTour}>
      <QuestionMark size={25} />
    </div>
  );
}

StartTourIcon.propTypes = { onStartTour: PropTypes.func.isRequired };

export { StartTourIcon };
