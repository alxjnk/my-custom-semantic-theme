export const countOfDayCellsInCalendar = 42;
export const countOfDayRows = 6;
export const countOfDaysInRows = 7;
export const enWeekDays = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
export const ruWeekDays = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
