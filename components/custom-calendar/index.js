import moment from 'moment';
import React from 'react';
import PropTypes from 'prop-types';
import { enWeekDays, ruWeekDays } from './constants';
import { getArrayOfDates } from './utils';
import { CustomCalendarView } from './components/custom-calendar-view';

function CustomCalendar({
  selectedDate,
  onChangeDate,
  localization,
  onSetFirstDayOfViewMonth,
  firstDayOfViewMonth,
  markedDate,
  minDate,
  maxDate,
  customCalendarCell,
}) {
  const rowsData = getArrayOfDates(firstDayOfViewMonth, markedDate);
  const weekDays = localization.includes('ru') ? ruWeekDays : enWeekDays;

  const handleSetNextMonth = () => {
    if (maxDate && moment(firstDayOfViewMonth).isBefore(moment(maxDate))) {
      return;
    }
    onSetFirstDayOfViewMonth(
      moment(firstDayOfViewMonth)
        .add(1, 'month')
        .format('YYYY-MM-DD'),
    );
  };

  const isPrevButtonDisabled = minDate
    ? moment(firstDayOfViewMonth).isSameOrBefore(moment(minDate))
    : false;

  const isNextButtonDisabled = maxDate
    ? moment(firstDayOfViewMonth).isBefore(moment(maxDate))
    : false;

  const handleSetPrevMonth = () => {
    if (
      minDate &&
      moment(firstDayOfViewMonth).isSameOrBefore(moment(minDate))
    ) {
      return;
    }
    onSetFirstDayOfViewMonth(
      moment(firstDayOfViewMonth)
        .subtract(1, 'month')
        .format('YYYY-MM-DD'),
    );
  };

  const centralHeaderText = moment(firstDayOfViewMonth)
    .locale(localization)
    .format('MMMM YYYY');

  return (
    <CustomCalendarView
      selectedDate={selectedDate}
      onChangeDate={onChangeDate}
      firstDayOfViewMonth={firstDayOfViewMonth}
      rowsData={rowsData}
      weekDays={weekDays}
      centralHeaderText={centralHeaderText}
      onSetPrevMonth={handleSetPrevMonth}
      onSetNextMonth={handleSetNextMonth}
      minDate={minDate}
      isPrevButtonDisabled={isPrevButtonDisabled}
      isNextButtonDisabled={isNextButtonDisabled}
      customCalendarCell={customCalendarCell}
    />
  );
}

CustomCalendar.propTypes = {
  markedDate: PropTypes.object.isRequired,
  selectedDate: PropTypes.string.isRequired,
  onChangeDate: PropTypes.func.isRequired,
  localization: PropTypes.string.isRequired,
  onSetFirstDayOfViewMonth: PropTypes.func.isRequired,
  firstDayOfViewMonth: PropTypes.string.isRequired,
  minDate: PropTypes.string,
  maxDate: PropTypes.string,
  customCalendarCell: PropTypes.elementType,
};

export { CustomCalendar };
