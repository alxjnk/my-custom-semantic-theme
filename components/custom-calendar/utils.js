import moment from 'moment';
import {
  countOfDayCellsInCalendar,
  countOfDayRows,
  countOfDaysInRows,
} from './constants';

export const getArrayOfDates = (startOfMonth, markedDate) => {
  const firstDayOfCalendar = moment(startOfMonth).startOf('week');
  const lastDayOfCalendar = moment(firstDayOfCalendar).add(
    countOfDayCellsInCalendar,
    'day',
  );
  const dates = [];
  // push day in dates array
  while (moment(lastDayOfCalendar).diff(firstDayOfCalendar, 'days') >= 1) {
    const selectedDay = firstDayOfCalendar.format('YYYY-MM-DD');
    const dayMarkers = markedDate[selectedDay] || [];
    dates.push({ date: selectedDay, dayMarkers });
    firstDayOfCalendar.add(1, 'days');
  }

  // return separated array by rows
  return new Array(countOfDayRows)
    .fill()
    .map(_ => dates.splice(0, countOfDaysInRows));
};
