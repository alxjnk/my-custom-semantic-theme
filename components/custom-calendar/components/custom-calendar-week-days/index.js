import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

function CustomCalendarWeekDays({ weekDays }) {
  return (
    <div className="custom-calendar-week-days">
      {weekDays.map(day => (
        <div className="custom-calendar-week-days__day-of-week" key={day}>
          {day}
        </div>
      ))}
    </div>
  );
}

CustomCalendarWeekDays.propTypes = {
  weekDays: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
};

export { CustomCalendarWeekDays };
