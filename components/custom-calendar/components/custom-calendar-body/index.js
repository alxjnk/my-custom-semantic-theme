import React from 'react';
import PropTypes from 'prop-types';
import { CalendarCell } from '../calendar-cell';
import './styles.less';

function CustomCalendarBody({
  rowsData,
  selectedDate,
  onChangeDate,
  firstDayOfViewMonth,
  minDate,
  customCalendarCell: CustomCalendarCell,
}) {
  return (
    <div className="custom-calendar-body">
      {rowsData.map((row, rowIndex) => (
        <div className="custom-calendar-body__row" key={rowIndex.toString()}>
          {row.map((item, index) =>
            CustomCalendarCell ? (
              <CustomCalendarCell
                data={item}
                selectedDate={selectedDate}
                onChangeDate={onChangeDate}
                firstDayOfViewMonth={firstDayOfViewMonth}
                key={index.toString()}
                minDate={minDate}
              />
            ) : (
              <CalendarCell
                data={item}
                selectedDate={selectedDate}
                onChangeDate={onChangeDate}
                firstDayOfViewMonth={firstDayOfViewMonth}
                key={index.toString()}
                minDate={minDate}
              />
            ),
          )}
        </div>
      ))}
    </div>
  );
}

CustomCalendarBody.propTypes = {
  rowsData: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        date: PropTypes.string.isRequired,
        dayMarkers: PropTypes.arrayOf(
          PropTypes.oneOf(['green', 'blue', 'orange']),
        ).isRequired,
      }),
    ),
  ),
  selectedDate: PropTypes.string.isRequired,
  onChangeDate: PropTypes.func.isRequired,
  firstDayOfViewMonth: PropTypes.string.isRequired,
  minDate: PropTypes.string,
  customCalendarCell: PropTypes.elementType,
};

export { CustomCalendarBody };
