import React from 'react';
import PropTypes from 'prop-types';
import { ChevronLeft, ChevronRight } from 'tabler-icons-react';
import './styles.less';

function CustomCalendarHeader({
  centralText,
  onClickPrev,
  onClickNext,
  isPrevButtonDisabled,
  isNextButtonDisabled,
}) {
  return (
    <div className="custom-calendar-header">
      <div
        className={`custom-calendar-header__chevron ${
          isPrevButtonDisabled ? 'custom-calendar-header__chevron_disabled' : ''
        }`}
      >
        <ChevronLeft onClick={onClickPrev} />
      </div>
      <div className="custom-calendar-header__text">{centralText}</div>
      <div
        className={`custom-calendar-header__chevron ${
          isNextButtonDisabled ? 'custom-calendar-header__chevron_disabled' : ''
        }`}
      >
        <ChevronRight onClick={onClickNext} />
      </div>
    </div>
  );
}

CustomCalendarHeader.propTypes = {
  centralText: PropTypes.string.isRequired,
  onClickPrev: PropTypes.func.isRequired,
  onClickNext: PropTypes.func.isRequired,
  isPrevButtonDisabled: PropTypes.bool.isRequired,
  isNextButtonDisabled: PropTypes.bool.isRequired,
};

export { CustomCalendarHeader };
