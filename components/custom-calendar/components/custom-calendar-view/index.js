import React from 'react';
import PropTypes from 'prop-types';
import { CustomCalendarBody } from '../custom-calendar-body';
import { CustomCalendarHeader } from '../custom-calendar-header';
import { CustomCalendarWeekDays } from '../custom-calendar-week-days';
import './styles.less';

function CustomCalendarView({
  selectedDate,
  onChangeDate,
  firstDayOfViewMonth,
  rowsData,
  weekDays,
  centralHeaderText,
  onSetPrevMonth,
  onSetNextMonth,
  minDate,
  isPrevButtonDisabled,
  isNextButtonDisabled,
  customCalendarCell,
}) {
  return (
    <div className="custom-calendar-view">
      <CustomCalendarHeader
        centralText={centralHeaderText}
        onClickPrev={onSetPrevMonth}
        onClickNext={onSetNextMonth}
        isPrevButtonDisabled={isPrevButtonDisabled}
        isNextButtonDisabled={isNextButtonDisabled}
      />
      <CustomCalendarWeekDays weekDays={weekDays} />
      <CustomCalendarBody
        rowsData={rowsData}
        selectedDate={selectedDate}
        onChangeDate={onChangeDate}
        firstDayOfViewMonth={firstDayOfViewMonth}
        minDate={minDate}
        customCalendarCell={customCalendarCell}
      />
    </div>
  );
}

CustomCalendarView.propTypes = {
  selectedDate: PropTypes.string.isRequired,
  onChangeDate: PropTypes.func.isRequired,
  firstDayOfViewMonth: PropTypes.string.isRequired,
  rowsData: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        date: PropTypes.string.isRequired,
        dayMarkers: PropTypes.arrayOf(
          PropTypes.oneOf(['green', 'blue', 'orange']),
        ).isRequired,
      }),
    ),
  ),
  weekDays: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  centralHeaderText: PropTypes.string.isRequired,
  onSetPrevMonth: PropTypes.func.isRequired,
  onSetNextMonth: PropTypes.func.isRequired,
  minDate: PropTypes.string,
  isPrevButtonDisabled: PropTypes.bool.isRequired,
  isNextButtonDisabled: PropTypes.bool.isRequired,
  customCalendarCell: PropTypes.elementType,
};

export { CustomCalendarView };
