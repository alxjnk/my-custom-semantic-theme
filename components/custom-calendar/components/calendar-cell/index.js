import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { getIsDisabled } from './utils';
import { CustomCalendarCellView } from '../custom-calendar-cell-view';

function CalendarCell({
  data,
  selectedDate,
  onChangeDate,
  firstDayOfViewMonth,
  minDate,
}) {
  const cellText = moment(data.date).format('D');

  const isDisabled = useMemo(
    () => getIsDisabled(data.date, minDate, firstDayOfViewMonth),
    [data.date, firstDayOfViewMonth, minDate],
  );

  const isSameDay = useMemo(() => moment(data.date).isSame(moment(), 'day'), [
    data.date,
  ]);

  const isSelectedDay = useMemo(
    () => moment(data.date).isSame(moment(selectedDate), 'day'),
    [data.date, selectedDate],
  );

  const handleChangeDate = () => {
    if (isDisabled) {
      return;
    }
    onChangeDate(data.date);
  };

  return (
    <CustomCalendarCellView
      text={cellText}
      isDisabled={isDisabled}
      isSameDay={isSameDay}
      isSelectedDay={isSelectedDay}
      onChangeDate={handleChangeDate}
      markers={data.dayMarkers}
    />
  );
}

CalendarCell.propTypes = {
  data: PropTypes.shape({
    date: PropTypes.string.isRequired,
    dayMarkers: PropTypes.arrayOf(PropTypes.oneOf(['green', 'blue', 'orange']))
      .isRequired,
  }),
  selectedDate: PropTypes.string.isRequired,
  onChangeDate: PropTypes.func.isRequired,
  firstDayOfViewMonth: PropTypes.string.isRequired,
  minDate: PropTypes.string,
};

export { CalendarCell };
