import moment from 'moment';

export const getIsDisabled = (date, minDate, firstDayOfViewMonth) => {
  if (!moment(date).isSame(moment(firstDayOfViewMonth), 'month')) {
    return true;
  }
  if (minDate && moment(date).isBefore(moment(minDate))) {
    return true;
  }
  return false;
};
