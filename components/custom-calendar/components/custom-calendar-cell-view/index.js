/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const CLASS_NAME = 'custom-calendar-cell-view';

function CustomCalendarCellView({
  text,
  isDisabled,
  isSameDay,
  isSelectedDay,
  onChangeDate,
  markers,
}) {
  return (
    <div
      className={`${CLASS_NAME} ${isDisabled ? `${CLASS_NAME}_disabled` : ''} ${
        isSameDay ? `${CLASS_NAME}_today` : ''
      } ${isSelectedDay ? `${CLASS_NAME}_selected` : ''}`}
      onClick={onChangeDate}
    >
      <p className={`${CLASS_NAME}__text`}>{text}</p>
      {markers && (
        <div className={`${CLASS_NAME}__markers`}>
          {markers.map(marker => (
            <div key={marker} className={`${CLASS_NAME}__marker-wrapper`}>
              <div
                className={`${CLASS_NAME}__marker ${CLASS_NAME}__marker_color_${marker}`}
              />
            </div>
          ))}
        </div>
      )}
    </div>
  );
}

CustomCalendarCellView.propTypes = {
  text: PropTypes.string.isRequired,
  isDisabled: PropTypes.bool.isRequired,
  isSameDay: PropTypes.bool.isRequired,
  isSelectedDay: PropTypes.bool.isRequired,
  onChangeDate: PropTypes.func.isRequired,
  markers: PropTypes.arrayOf(PropTypes.oneOf(['green', 'blue', 'orange']))
    .isRequired,
};

export { CustomCalendarCellView };
