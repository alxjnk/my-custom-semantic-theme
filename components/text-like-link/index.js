/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

function TextLikeLink({ text, onClick }) {
  return (
    <p className="text-like-link" onClick={onClick}>
      {text}
    </p>
  );
}

TextLikeLink.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export { TextLikeLink };
