import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

function TableWrapper({ table }) {
  return <div className="table-wrapper">{table}</div>;
}

TableWrapper.propTypes = { table: PropTypes.element.isRequired };

export { TableWrapper };
