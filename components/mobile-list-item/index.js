/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';

const MobileListItem = ({
  avatarText,
  title,
  subText,
  avatar: Avatar,
  borderColor,
  onItemTextClick,
  icon,
  img,
}) => (
  <div
    className={`mobile-list-item ${
      borderColor ? `mobile-list-item_border-${borderColor}` : ''
    }`}
  >
    {Avatar && (
      <div className="mobile-list-item__avatar">
        <Avatar name={avatarText} img={img} />
      </div>
    )}
    <div className="mobile-list-item__info" onClick={onItemTextClick}>
      <p className="mobile-list-item__info-text">{title}</p>
      {subText && <p className="mobile-list-item__info-subtext">{subText}</p>}
    </div>
    {icon && <div className="mobile-list-item__icon">{icon}</div>}
  </div>
);

MobileListItem.propTypes = {
  avatarText: PropTypes.string,
  subText: PropTypes.string,
  img: PropTypes.string,
  borderColor: PropTypes.oneOf(['blue', 'green', 'purple', 'orange', 'red']),
  onItemTextClick: PropTypes.func,
  icon: PropTypes.element,
  avatar: PropTypes.element,
  title: PropTypes.string.isRequired,
};

export { MobileListItem };
