import React from 'react';
import PropTypes from 'prop-types';
import { DateTimeInput } from 'semantic-ui-calendar-react';
import './styles.less';

const FormDateTimePickerField = ({
  input,
  placeholder,
  meta: { error, dirty },
  label,
  minDate,
  maxDate,
}) => {
  const handleChangeTime = (_, { value }) => {
    input.onChange(value);
  };
  return (
    <div className="form-date-time-picker-field">
      {label && <label htmlFor={label}>{label}</label>}
      <DateTimeInput
        value={input.value}
        animation="none"
        onChange={handleChangeTime}
        placeholder={placeholder}
        popupPosition="bottom center"
        dateFormat="YYYY-MM-DD"
        closable
        id={label}
        className="form-date-time-picker-field__date-input"
        minDate={minDate}
        maxDate={maxDate}
        fluid
      />
      {dirty && (error && <span className="validation_error">{error}</span>)}
    </div>
  );
};

FormDateTimePickerField.propTypes = {
  input: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  label: PropTypes.string,
  maxDate: PropTypes.string,
  minDate: PropTypes.string,
};

export { FormDateTimePickerField };
