import React from 'react';
import { Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import './styles.less';

const FormInputFieldView = ({
  input,
  type,
  placeholder,
  shouldErrorBeShown,
  errorMessage,
  label,
  autoFocus,
  autoComplete,
}) => (
  <div className="input_row">
    {label && <label htmlFor={label}>{label}</label>}

    <Input
      {...input}
      value={input.value}
      autoFocus={autoFocus}
      fluid
      type={type}
      placeholder={placeholder}
      id={label}
      autoComplete={autoComplete}
    />
    {shouldErrorBeShown && (
      <span className="validation_error">{errorMessage}</span>
    )}
  </div>
);

FormInputFieldView.propTypes = {
  input: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired,
  shouldErrorBeShown: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  autoFocus: PropTypes.bool,
  autoComplete: PropTypes.oneOf(['off']),
};

export { FormInputFieldView };
