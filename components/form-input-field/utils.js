export const getShouldErrorBeShown = (notShow, touched, error) => {
  if (notShow) {
    return false;
  }
  if (notShow === false) {
    return Boolean(error);
  }
  if (touched && error) {
    return touched && Boolean(error);
  }
  return false;
};
