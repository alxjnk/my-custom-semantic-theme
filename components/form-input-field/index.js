import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { FormInputFieldView } from './components/form-input-field-view';
import { getShouldErrorBeShown } from './utils';

const FormInputField = ({
  input,
  type,
  placeholder,
  meta: { touched, error },
  label,
  autoFocus,
  autoComplete,
  errorsNotShown,
}) => {
  const shouldErrorBeShown = useMemo(
    () => getShouldErrorBeShown(errorsNotShown, touched, error),
    [errorsNotShown, touched, error],
  );

  return (
    <FormInputFieldView
      shouldErrorBeShown={shouldErrorBeShown}
      autoFocus={autoFocus}
      autoComplete={autoComplete}
      label={label}
      placeholder={placeholder}
      type={type}
      input={input}
      errorMessage={error}
    />
  );
};

FormInputField.propTypes = {
  input: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  autoFocus: PropTypes.bool,
  autoComplete: PropTypes.oneOf(['off']),
  errorsNotShown: PropTypes.bool,
};

export { FormInputField };
