import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { AlertCircle } from 'tabler-icons-react';
import './styles.less';

function ErrorBoundaryView({ onRefreshPage }) {
  return (
    <div className="error-boundary-view">
      <div className="error-boundary-view__content">
        <AlertCircle size={40} />
        <h2 className="error-boundary-view__content-text">
          Oops! Something went wrong. Please refresh page
        </h2>
        <Button primary onClick={onRefreshPage}>
          Click for refresh page
        </Button>
      </div>
    </div>
  );
}

ErrorBoundaryView.propTypes = {
  onRefreshPage: PropTypes.func.isRequired,
};

export { ErrorBoundaryView };
