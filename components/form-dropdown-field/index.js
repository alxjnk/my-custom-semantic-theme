import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { FormDropdownFieldView } from './components/form-dropdown-field-view';
import { getShouldErrorBeShown } from '../form-input-field/utils';

function FormDropdownField({
  placeholder,
  input,
  options,
  meta: { dirty, error },
  label,
  search,
  multiple,
  onSearchChange,
  searchQuery,
  index,
  noResultsMessage,
  errorsNotShown,
}) {
  const handleChangeValue = (_, data) => {
    input.onChange(data.value);
    if (onSearchChange) {
      onSearchChange(data, index, data.value);
    }
  };
  const handleSearchChange = (_, data) => {
    if (onSearchChange) {
      onSearchChange(data, index);
    }
  };
  const shouldErrorBeShown = useMemo(
    () => getShouldErrorBeShown(errorsNotShown, dirty, error),
    [errorsNotShown, dirty, error],
  );

  return (
    <FormDropdownFieldView
      label={label}
      value={input.value}
      placeholder={placeholder}
      search={search}
      options={options}
      onChange={handleChangeValue}
      multiple={multiple}
      onSearchChange={handleSearchChange}
      searchQuery={searchQuery}
      noResultsMessage={noResultsMessage}
      error={error}
      shouldErrorBeShown={shouldErrorBeShown}
    />
  );
}

FormDropdownField.propTypes = {
  placeholder: PropTypes.string,
  input: PropTypes.object,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      key: PropTypes.string.isRequired,
    }),
  ),
  meta: PropTypes.object.isRequired,
  label: PropTypes.string,
  search: PropTypes.bool,
  multiple: PropTypes.bool,
  onSearchChange: PropTypes.func,
  searchQuery: PropTypes.string,
  index: PropTypes.number,
  noResultsMessage: PropTypes.string,
  errorsNotShown: PropTypes.bool,
};

export { FormDropdownField };
