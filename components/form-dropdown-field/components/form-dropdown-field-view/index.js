import React from 'react';
import PropTypes from 'prop-types';
import './styles.less';
import { Dropdown } from 'semantic-ui-react';

function FormDropdownFieldView({
  label,
  value,
  placeholder,
  search,
  options,
  onChange,
  multiple,
  onSearchChange,
  searchQuery,
  noResultsMessage,
  shouldErrorBeShown,
  error,
}) {
  return (
    <div className="form-dropdown-field input_row">
      {label && <label htmlFor={label}>{label}</label>}
      <Dropdown
        value={value}
        fluid
        placeholder={placeholder}
        search={search}
        selection
        options={options}
        onChange={onChange}
        id={label}
        multiple={multiple}
        onSearchChange={onSearchChange}
        searchQuery={searchQuery}
        className="form-dropdown-field__dropdown"
        noResultsMessage={noResultsMessage}
      />
      {shouldErrorBeShown && <span className="validation_error">{error}</span>}
    </div>
  );
}

FormDropdownFieldView.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      key: PropTypes.string.isRequired,
    }),
  ),
  label: PropTypes.string,
  search: PropTypes.bool,
  multiple: PropTypes.bool,
  onSearchChange: PropTypes.func,
  searchQuery: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  noResultsMessage: PropTypes.string,
  shouldErrorBeShown: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

export { FormDropdownFieldView };
