import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { FormTimePickerView } from './components/form-time-picker-field';
import { getShouldErrorBeShown } from '../form-input-field/utils';

const FormTimePickerField = ({
  input,
  type,
  placeholder,
  meta: { touched, error },
  label,
  errorsNotShown,
}) => {
  const handleChangeTime = (_, { value }) => {
    input.onChange(value);
  };
  const shouldErrorBeShown = useMemo(
    () => getShouldErrorBeShown(errorsNotShown, touched, error),
    [errorsNotShown, touched, error],
  );
  return (
    <FormTimePickerView
      label={label}
      value={input.value}
      onChange={handleChangeTime}
      type={type}
      placeholder={placeholder}
      error={error}
      shouldErrorBeShown={shouldErrorBeShown}
    />
  );
};

FormTimePickerField.propTypes = {
  input: PropTypes.object.isRequired,
  placeholder: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  errorsNotShown: PropTypes.bool,
};

export { FormTimePickerField };
