import React from 'react';
import PropTypes from 'prop-types';
import { TimeInput } from 'semantic-ui-calendar-react';
import './styles.less';

function FormTimePickerView({
  label,
  value,
  onChange,
  type,
  placeholder,
  error,
  shouldErrorBeShown,
}) {
  return (
    <div className="form-time-picker-field">
      {label && <label htmlFor={label}>{label}</label>}
      <TimeInput
        value={value}
        animation="none"
        onChange={onChange}
        type={type}
        placeholder={placeholder}
        popupPosition="bottom center"
        closable
        id={label}
        className="form-time-picker-field__time-input"
        fluid
      />
      {shouldErrorBeShown && <span className="validation_error">{error}</span>}
    </div>
  );
}

FormTimePickerView.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  shouldErrorBeShown: PropTypes.bool.isRequired,
};

export { FormTimePickerView };
