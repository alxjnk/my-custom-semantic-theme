import React from 'react';
import PropTypes from 'prop-types';
import { DateTimeInput } from 'semantic-ui-calendar-react';

function CustomDateTimeInput({
  selectedDate,
  onChangeDate,
  markedDate,
  minDate,
  localization,
}) {
  return (
    <div className="custom-date-input">
      <DateTimeInput
        localization={localization}
        inline
        value={selectedDate}
        onChange={onChangeDate}
        marked={markedDate}
        minDate={minDate}
      />
    </div>
  );
}

CustomDateTimeInput.propTypes = {
  selectedDate: PropTypes.string.isRequired,
  onChangeDate: PropTypes.func.isRequired,
  markedDate: PropTypes.arrayOf(PropTypes.instanceOf(Date)),
  minDate: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  localization: PropTypes.string,
};

export { CustomDateTimeInput };
