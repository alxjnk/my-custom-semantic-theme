import React from 'react';
import PropTypes from 'prop-types';
import { Popup } from 'semantic-ui-react';
import './styles.less';

function CustomControlledPopup({
  position,
  trigger,
  isOpen,
  onClose,
  onOpen,
  header,
  content,
  withoutPadding,
}) {
  return (
    <Popup
      position={position}
      trigger={trigger}
      on="click"
      open={isOpen}
      onClose={onClose}
      onOpen={onOpen}
      className={`custom-controlled-popup ${
        withoutPadding ? 'custom-controlled-popup_padding-less' : ''
      }`}
    >
      <Popup.Header>{header}</Popup.Header>
      <Popup.Content>{content}</Popup.Content>
    </Popup>
  );
}

CustomControlledPopup.propTypes = {
  position: PropTypes.string,
  trigger: PropTypes.element.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onOpen: PropTypes.func.isRequired,
  header: PropTypes.element.isRequired,
  content: PropTypes.element.isRequired,
  withoutPadding: PropTypes.bool,
};

export { CustomControlledPopup };
